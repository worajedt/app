# Read Me

Licensed under the Apache License, Version 2.0

## Xojo

### What it is?
This is a project template for Xojo Web Application. Using [Xojo 2016r1.1][1] and [Docker 1.12.0-rc3][2]. The container will be running with Apache HTTPd Server 2.2 with "/cgi-bin/app.cgi" from Xojo. You can build Xojo project and then rebuild Docker image from the same project

[Quick start Xojo Web here.][3]

## What you have already?
Xojo Web Application has been named "app". The project file has been set to ..

### What you CAN'T change.
1. Auto Increment Version = True
2. Deployment Type = CGI + Automatically Choose Port
3. Use Builds Folder = On

### What you can change.
1. Language = Thai (You can change for sure)
2. Application Identifier = com.skoodeskill.app, You can also change it.

## Docker

### Dockerfile

I've use Container Registry for my docker image too. Check it out.

[https://gitlab.com/sjedt/app/container_registry][5]


**or**

Build docker image from the Dockerfile by using this command.
```
docker build -t ubuntu14xojo .
```

Then it should show the successful result like this for example.
```
Successfully built 919de89fec93
```

### Run the new docker

Check docker image.

```
docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu14xojo        latest              919de89fec93        26 minutes ago      468.5 MB
```

Run the new shinny docker
```
docker run -d -p80:80  -i -t ubuntu14xojo
```

Then check your browser by go to 
```
http://localhost
```

You should see the welcome page. For Xojo Web App, go to 
```
http://localhost/cgi-bin/app.cgi
```

## Who is maintaining this project?
Worajedt Sitthidumrong \<[sjedt@3ddaily.com][4]\> ... no blog, no website yet. Sorry.

[1]:	http://www.xojo.com
[2]:	https://www.docker.com/products/docker
[3]:	http://developer.xojo.com/web-quickstart
[4]:	mailto:sjedt@3ddaily.com
[5]:    https://gitlab.com/sjedt/app/container_registry
