FROM scratch
MAINTAINER Worajedt Sitthidumrong<sjedt@3ddaily.com>

ADD ubuntu-trusty-core-cloudimg-amd64-root.tar.gz /

# a few minor docker-specific tweaks
# see https://github.com/docker/docker/blob/master/contrib/mkimage/debootstrap
RUN set -xe \
	\
	&& echo '#!/bin/sh' > /usr/sbin/policy-rc.d \
	&& echo 'exit 101' >> /usr/sbin/policy-rc.d \
	&& chmod +x /usr/sbin/policy-rc.d \
	\
	&& dpkg-divert --local --rename --add /sbin/initctl \
	&& cp -a /usr/sbin/policy-rc.d /sbin/initctl \
	&& sed -i 's/^exit.*/exit 0/' /sbin/initctl \
	\
	&& echo 'force-unsafe-io' > /etc/dpkg/dpkg.cfg.d/docker-apt-speedup \
	\
	&& echo 'DPkg::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' > /etc/apt/apt.conf.d/docker-clean \
	&& echo 'APT::Update::Post-Invoke { "rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true"; };' >> /etc/apt/apt.conf.d/docker-clean \
	&& echo 'Dir::Cache::pkgcache ""; Dir::Cache::srcpkgcache "";' >> /etc/apt/apt.conf.d/docker-clean \
	\
	&& echo 'Acquire::Languages "none";' > /etc/apt/apt.conf.d/docker-no-languages \
	\
	&& echo 'Acquire::GzipIndexes "true"; Acquire::CompressionTypes::Order:: "gz";' > /etc/apt/apt.conf.d/docker-gzip-indexes

# delete all the apt list files since they're big and get stale quickly
RUN rm -rf /var/lib/apt/lists/*
# this forces "apt-get update" in dependent images, which is also good

# Let the conatiner know that there is no tty
ENV DEBIAN_FRONTEND noninteractive

# enable the universe
RUN sed -i 's/^#\s*\(deb.*universe\)$/\1/g' /etc/apt/sources.list
# RUN sed -i 's/^#\s*\(deb.*multiverse\)$/\1/g' /etc/apt/sources.list
RUN sed -i 's/archive.ubuntu.com/mirror1.ku.ac.th/g' /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y apt-utils

# Install essential packages
RUN cd /etc/apt/sources.list.d
RUN dpkg --add-architecture i386
RUN apt-get update
RUN apt-get install -y dialog
RUN apt-get install -y apache2 apache2-utils apachetop vim curl wget nano git ntp ntpdate less net-tools lynx libc6:i386 libncurses5:i386 libstdc++6:i386 libglib2.0-0:i386 libsoup2.4-1:i386 libicu52:i386 libgtk2.0-0:i386
RUN echo ":syntax on" >> /root/.vimrc

# Apache2
RUN a2dismod mpm_event && a2enmod mpm_prefork
RUN a2enmod cgid expires info ssl rewrite
ADD 000-default.conf /etc/apache2/sites-enabled/000-default.conf

# Copy a Xojo Web app to cgi-bin
# COPY can use with file, directory
# ADD can use with .tar file

ADD Builds*-*app.xojo_project/Linux/app /usr/lib/cgi-bin

RUN chown -R www-data /usr/lib/cgi-bin/*.*
RUN chmod -R 755 /usr/lib/cgi-bin/
RUN chmod 666 /usr/lib/cgi-bin/config.cfg

RUN mkdir -p /var/www/html
RUN chown -R www-data /var/www/html/
ADD index.html /var/www/html

RUN /usr/sbin/apachectl restart

# Environment setting
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

# Port setting
EXPOSE 80
EXPOSE 443

RUN /usr/sbin/apachectl stop

COPY apache2foreground.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/apache2foreground.sh

# Clean package cache
RUN apt-get clean ; rm -rf /var/cache/apk/*

# Entry point

# overwrite this with 'CMD []' in a dependent Dockerfile
CMD ["apache2-foreground"]
# CMD ["/bin/bash"]
